from urllib import request
import xml.etree.ElementTree as ET

uh = request.urlopen('https://www.cbr.ru/scripts/XML_daily.asp')
full = []
for v in ET.fromstring(uh.read()).findall('./Valute'):
    full.append((v.find('./CharCode').text,
                 v.find('./Value').text))

print(full)
